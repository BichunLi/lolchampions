import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-create-champ',
  templateUrl: './create-champ.component.html',
  styleUrls: ['./create-champ.component.css']
})
export class CreateChampComponent implements OnInit {
  @Input() champDetails = {id: 0, name: '', role: '', difficulty: '', imageUrl: ''}

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addChamp() {
    this.restApi.createChamp(this.champDetails).subscribe((data: {}) => {this.router.navigate(['/list-champs'])})
  }

}

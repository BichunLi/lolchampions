import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateChampComponent } from './create-champ.component';

describe('CreateChampComponent', () => {
  let component: CreateChampComponent;
  let fixture: ComponentFixture<CreateChampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateChampComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateChampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

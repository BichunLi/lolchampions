import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChampComponent } from './list-champ.component';

describe('ListChampComponent', () => {
  let component: ListChampComponent;
  let fixture: ComponentFixture<ListChampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListChampComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListChampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { LolChamp } from '../shared/lol-champ';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-list-champ',
  templateUrl: './list-champ.component.html',
  styleUrls: ['./list-champ.component.css'],
})
export class ListChampComponent implements OnInit {
  Champs: LolChamp[] = [];

  constructor(public restApi: RestApiService) {}

  ngOnInit(): void {
    this.loadChamps();
  }

  loadChamps() {
    return this.restApi.getChamps().subscribe((data: LolChamp[]) => {
      this.Champs = data;
    })
  }

  deleteChamp(id: any) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.restApi.deleteChamp(id).subscribe(data => {
        this.loadChamps()
      })
    }
  }
}

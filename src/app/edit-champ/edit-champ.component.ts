import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-edit-champ',
  templateUrl: './edit-champ.component.html',
  styleUrls: ['./edit-champ.component.css'],
})
export class EditChampComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  champDetails: any = {};

  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.restApi.getChamp(this.id).subscribe((data: {}) => {
      this.champDetails = data;
    });
  }

  updateChamp() {
    if (window.confirm('Are you sure you want to update?')) {
      this.restApi.editChamp(this.id, this.champDetails).subscribe((data) => {
        this.router.navigate(['/list-champ']);
      });
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChampComponent } from './edit-champ.component';

describe('EditChampComponent', () => {
  let component: EditChampComponent;
  let fixture: ComponentFixture<EditChampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditChampComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateChampComponent } from './create-champ/create-champ.component';
import { EditChampComponent } from './edit-champ/edit-champ.component';
import { ListChampComponent } from './list-champ/list-champ.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'list-champ'},
  {path: 'list-champ', component: ListChampComponent},
  {path: 'create-champ', component: CreateChampComponent},
  {path: 'edit-champ/:id', component: EditChampComponent},
  {path: '**', component: ListChampComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

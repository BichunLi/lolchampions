import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LolChamp } from './lol-champ';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RestApiService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) {}

  // HTTP Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // HTTP GET
  getChamps(): Observable<LolChamp[]> {
    return this.http
      .get<LolChamp[]>(this.apiURL)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HTTP GET
  getChamp(id: any): Observable<LolChamp> {
    return this.http
      .get<LolChamp>(this.apiURL + '/' + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HTTP PUT
  editChamp(id: number, champ: LolChamp): Observable<LolChamp> {
    return this.http
      .put<LolChamp>(this.apiURL + '', JSON.stringify(champ), this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HTTP POST
  createChamp(champ: LolChamp): Observable<LolChamp> {
    return this.http
      .post<LolChamp>(this.apiURL + '', JSON.stringify(champ), this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HTTP DELETE
  deleteChamp(id: number) {
    return this.http
      .delete<LolChamp>(this.apiURL + id, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListChampComponent } from './list-champ/list-champ.component';
import { CreateChampComponent } from './create-champ/create-champ.component';
import { EditChampComponent } from './edit-champ/edit-champ.component';

@NgModule({
  declarations: [
    AppComponent,
    ListChampComponent,
    CreateChampComponent,
    EditChampComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
